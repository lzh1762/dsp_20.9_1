
//Serial和Serial1对象都支持5，6，7，8个数据位，奇数（O）、偶数（E）和无（N）奇偶校验，以
//及1或者2个停止位。
//Serial.begin(baudrate, SERIAL_8N1), Serial.begin(baudrate, SERIAL_6E2)
//Serial.begin(baudrate, SERIAL_8O2)

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define wifi_ssid "Meizu5"              //连接热点
#define wifi_password "731801789"
#define mqtt_server "183.230.40.39"                         //OneNet-MQTT IP
#define mqtt_port 6002                                      //OneNet-MQTT port
#define mqtt_devid "621408218"   //设备ID    连接DSP
#define mqtt_pubid "323078"      //产品ID
#define mqtt_password "xxx"          //OneNet-MQTT的 APIkey 这里用XXX代替
#define LED 2     //灯  的pin脚

long lastMsg = 0; //上一次发送时间

  //0 一般设置
  bool Bypass=1,LED_work=1;   //直通？   板子led
  int VOL_num=100;    //音量
  //1 声向
  bool V_sound_work=0;   //是否工作
  int time10ms=30,range=36;   //设置时间间隔(发送)、范围
  //2 EQ
  bool EQ_work=0;
  int Lfc=3000,LQ=2,LG=0;   //频率 Q 增益（发送给单片机要加50！） 
  int Rfc=8000,RQ=2,RG=0;
  //3 
  bool Reverb_work=0;
  int rt60=12,alpha=25;   //混响时间s（发送*10） alpha  (发送*100)
  
WiFiClient espClient;
PubSubClient client(espClient);
void up_state() {

  //调整time, range, rt60,alpha
  SendMQTT_onestring("state","[ VOL_num:"+String(VOL_num)+", Bypass:"+String(Bypass)+", LED:"+String(LED_work)+"]"); 
  SendMQTT_onestring("function1","[ V_sound:"+String(V_sound_work)+", time:"+String((float)time10ms/100.0)+", range:"+String(range*10)+"]"); 
  SendMQTT_onestring("function2","[ EQ:"+String(EQ_work)+", 1fc:"+String(Lfc)+", 1Q:"+String(LQ)+", 1G:"+String(LG)+", 2fc:"+String(Rfc)+", 2Q:"+String(RQ)+", 2G:"+String(RG)+"]"); 
  SendMQTT_onestring("function3","[ Reverb:"+String(Reverb_work)+", rt60:"+String((float)rt60/10.0)+", alpha:"+String((float)alpha/100.0)+"]"); 
}

  
void setup() {

  pinMode(LED, OUTPUT);
  digitalWrite(LED, LED_work);
  //Serial.begin(115200);
  Serial.begin(115200, SERIAL_8O2); //8位  ODD奇校验  2 停止位



  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500); Serial.print(".");
  }
  Serial.println(" WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {    //接收到的  数据，很快反应
  bool function0=0,function1=0,function2=0,function3=0;  //用于  标记合并发送串口命令的  发给DSP

  String getmessage;
  //类似   LED:1,report:0,       以，结尾
  for (int i = 0; i < length; i++)  getmessage = getmessage + (char)payload[i];   //合成字符串
  //Serial.println(getmessage);  //接收到的命令  全部内容
  //  SendMQTT_onestring("state", getmessage);
  int place_temp = -1;
  //以下是 分析接收到的命令 并 执行

  //对应命令0
  if ((place_temp = getmessage.indexOf("Bypass")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    Bypass = value.toInt(); 
    function0=1;
    //Serial.print("#0,"+String(Bypass)+","+String(VOL_num)+","+String(LED_work)+"*");  //待改  统一发送！！
  }
  if ((place_temp = getmessage.indexOf("VOL_num")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    VOL_num = value.toInt(); 
    function0=1;
    //Serial.print("#0,"+String(Bypass)+","+String(VOL_num)+","+String(LED_work)+"*");  //待改
  }
  if ((place_temp = getmessage.indexOf("LED")) != -1) {  //控制LED
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get LED:" + value);
    LED_work = value.toInt();
    digitalWrite(LED, LED_work);
    function0=1;
    //Serial.print("#0,"+String(Bypass)+","+String(VOL_num)+","+String(LED_work)+"*");  //待改
  }
  //对应命令1  声向
  if ((place_temp = getmessage.indexOf("V_sound")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    V_sound_work = value.toInt(); 
    function1=1;
    }
  if ((place_temp = getmessage.indexOf("time")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    time10ms = value.toInt(); 
    function1=1;
    }
  if ((place_temp = getmessage.indexOf("range")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    range = value.toInt(); 
    function1=1;
    }
  //命令2  EQ
  if ((place_temp = getmessage.indexOf("EQ")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    EQ_work = value.toInt(); 
    function2=1;
    }
  if ((place_temp = getmessage.indexOf("1fc")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    Lfc = value.toInt(); 
    function2=1;
    }
  if ((place_temp = getmessage.indexOf("1Q")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    LQ = value.toInt(); 
    function2=1;
    }
  if ((place_temp = getmessage.indexOf("1G")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    LG = value.toInt(); 
    function2=1;
    } 
  if ((place_temp = getmessage.indexOf("2fc")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    Rfc = value.toInt(); 
    function2=1;
    }
  if ((place_temp = getmessage.indexOf("2Q")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    RQ = value.toInt(); 
    function2=1;
    }
  if ((place_temp = getmessage.indexOf("2G")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    RG = value.toInt(); 
    function2=1;
    }
    //命令3   混响
  if ((place_temp = getmessage.indexOf("Reverb")) != -1) {  
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    Reverb_work = value.toInt(); 
    function3=1;
    }
  if ((place_temp = getmessage.indexOf("rt60")) != -1) {    
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    rt60 = value.toInt(); 
    function3=1;
    }
  if ((place_temp = getmessage.indexOf("alpha")) != -1) {    
    String value = getmessage.substring(getmessage.indexOf(':', place_temp) + 1, getmessage.indexOf(',', place_temp));
    //Serial.println("get data:" + value);
    alpha = value.toInt(); 
    function3=1;
    }
    

String temp1,temp2;
if(function0==1){
  if(VOL_num<10) temp1="00"+String(VOL_num);    //3位数
  else if(VOL_num<100)  temp1="0"+String(VOL_num);
  else temp1=String(VOL_num);
  Serial.println("#0,"+String(Bypass)+","+temp1+","+String(LED_work)+"*");  
}
if(function1==1){
  if(time10ms<10) temp1="0"+String(time10ms);
  else temp1=String(time10ms);
  if(range<10) temp2="0"+String(range);
  else temp2=String(range);
  Serial.println("#1,"+String(V_sound_work)+","+temp1+","+temp2+"*");  
}
if(function2==1){
  String temp3,temp4;
  if(Lfc<10) temp1="0000"+String(Lfc);    //5位数
  else if(Lfc<100)  temp1="000"+String(Lfc);
  else if(Lfc<1000)  temp1="00"+String(Lfc);
  else if(Lfc<10000)  temp1="0"+String(Lfc);
  else temp1=String(Lfc);
  if(LG<10) temp2="0"+String(LG);
  else temp2=String(LG);
  if(Rfc<10) temp3="0000"+String(Rfc);    //5位数
  else if(Rfc<100)  temp3="000"+String(Rfc);
  else if(Rfc<1000)  temp3="00"+String(Rfc);
  else if(Rfc<10000)  temp3="0"+String(Rfc);
  else temp3=String(Rfc);
  if(RG<10) temp4="0"+String(RG);
  else temp4=String(RG);
  
  Serial.println("#2,"+String(EQ_work)+","+temp1+","+String(LQ)+","+temp2+","+temp3+","+String(RQ)+","+temp4+"*");  
}
if(function3==1){
  if(rt60<10) temp1="0"+String(rt60);
  else temp1=String(rt60);
  if(alpha<10) temp2="0"+String(alpha);
  else temp2=String(alpha);
  Serial.println("#3,"+String(Reverb_work)+","+temp1+","+temp2+"*");  
}
  up_state();    //上传state
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect(mqtt_devid, mqtt_pubid, mqtt_password)) {
      Serial.println("connected");
      up_state();   //上报情况
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5s");
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected())     reconnect();

  delay(100);  //延时  可以不用  降低刷新率
  client.loop();   //检测 是否收到数据

  long now = millis();
  if (now - lastMsg > 6000) {  //20s上传数据
    lastMsg = now;

  }
}


bool SendMQTT_onestring(String stream_id1 , String string1) {
  return Send_onedatapoint_MQTT(stream_id1 ,  string1 , 1);
}
bool SendMQTT_onevalue(String stream_id0 , String string0) {
  return Send_onedatapoint_MQTT(stream_id0 ,  string0 , 0);
}
bool Send_onedatapoint_MQTT(String stream_id , String string , char flag)   //flag=1  发送字符串  0  数值
{
  String senddata;
  if (flag == 1)  senddata = "{\"" + stream_id + "\":\"" + string + "\"}";
  else  senddata = "{\"" + stream_id + "\":" + string + "}";
  int json_len = senddata.length();
  char msgJson[json_len + 3];   //实际+1
  char msg_buf[json_len + 10];
  msg_buf[0]=char(0x03);
  msg_buf[1]=char(json_len >> 8);
  msg_buf[2]=char(json_len & 0xff);
  senddata.toCharArray(msgJson, json_len + 1); //？
  memcpy(msg_buf + 3, msgJson, json_len);  //将msgJson数组的  字节 复制到msg_buf + 3上及以后
  //设备完成连接鉴权之后，将数据按照一定的格式（见协议文档说明）打包，将数据发布到$dp系统Topic上即可。
  return client.publish("$dp", (uint8_t*)msg_buf, 3 + json_len);    //// msg_buf as payload length which may have a "0x00"byte
}


