/////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2009 - Analog Devices, Inc.                                               //
//                                                                                         //
// NAME:     main.c (sample-based Talkthrough)                                             //
// DATE:     02/06/10                                                                      //
// PURPOSE:  Function main() for AD1939/ADSP-21489 Talkthrough framework.                  //
//                                                                                         //
// USAGE:    This file contains the main routine calls functions to set up the talkthrough //
//           routine.                                                                      //
//                                                                                         //
//                                                                                         //
// Author(s):  VS., Analog Devices, Inc                                                    //
//                                                                                         //
//                                                                                         //
//                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////

#include "ADDS_21489_EzKit.h"

#include "math.h"
#define COMB_N  4            // 梳状滤波器个数
int   delays[COMB_N]={2191,2971,3253,3307};// 梳状滤波器延时参数
float w[COMB_N]={0};    //参量
float kp[COMB_N]={0};   //参量
float Bp[COMB_N]={0};   //参量
float beta[COMB_N]={0};  //参量
int   ones[COMB_N]={1,1,1,1};   //参量

float rt60=0;/*混响时间*/
int   fs=0;  /*采样频率*/
float alpha=0;


void	main()
{
	initPLL_SDRAM(); //Initialize the PLL and SDRAM controller
    // Initialize DAI because the SPORT and SPI signals
    // need to be routed
    InitDAI();
    // This function will configure the AD1939 codec on the 21489 EZ-KIT
    Init1939viaSPI();
    // Turn on SPORT0 TX and SPORT1 RX for Multichannel Operation
    InitSPORT();
	// Unmask SPORT1 RX ISR Interrupt 
    interrupt(SIG_SP1,TalkThroughISR);
    
	int i;
	// 变量初始化	
    rt60=1.0;/*混响时间*/
    fs=96000;  /*采样频率*/
    alpha=0.25;
    for(i=0;i<COMB_N;i++)	// 计算参数	
    {  
 	    w[i]=(-3*delays[i])/(rt60*fs);
        kp[i]=pow(10,w[i]);
        Bp[i]=ones[i]-2/(1+pow(kp[i],(1-1/alpha)));
        beta[i]=kp[i]*(1-Bp[i]);
    }   
    
    
    // Be in infinite loop and do nothing until done.
	while(1)
	{
		if(inputReady)
			handleCodecData(buffer_cntr);
	}
}

