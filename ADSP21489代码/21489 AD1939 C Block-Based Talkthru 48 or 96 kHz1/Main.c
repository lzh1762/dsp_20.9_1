/////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2009 - Analog Devices, Inc.                                               //
//                                                                                         //
// NAME:     main.c (sample-based Talkthrough)                                             //
// DATE:     02/06/10                                                                      //
// PURPOSE:  Function main() for AD1939/ADSP-21489 Talkthrough framework.                  //
//                                                                                         //
// USAGE:    This file contains the main routine calls functions to set up the talkthrough //
//           routine.                                                                      //
//                                                                                         //
//                                                                                         //
// Author(s):  VS., Analog Devices, Inc                                                    //
//                                                                                         //
//                                                                                         //
//                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////

#include "ADDS_21489_EzKit.h"

#include <stdio.h>
#include <def21489.h>
//#include <signal.h>
#include <21489.h>

#define TAPS 512
float stateL[TAPS+1];
float stateR[TAPS+1];
int i;

int timer_count = 1;
int timeout_falg = 0;  //用于定时时间到的标记 
void timer_isr (int sig)  //定时器中断服务函数
{
    //printf("Timer interrupt number %d\n",timer_count);
    timer_count++;
    if(timer_count>=30){  //
    	timeout_falg++;
    	if(timeout_falg>=36){   // 0-35代表0-350度
    		timeout_falg=0;  //清空计数值
    	}
    	timer_count=0;  //清空计数值
    }
}

void	main()
{
		
	initPLL_SDRAM(); //Initialize the PLL and SDRAM controller
    // Initialize DAI because the SPORT and SPI signals
    // need to be routed
    InitDAI();
    // This function will configure the AD1939 codec on the 21489 EZ-KIT
    Init1939viaSPI();
    // Turn on SPORT0 TX and SPORT1 RX for Multichannel Operation
    InitSPORT();
	// Unmask SPORT1 RX ISR Interrupt 
    interrupt(SIG_SP1,TalkThroughISR);
    
	interrupts (SIG_TMZ, timer_isr);  //定时器中断设置
    timer_set(4000000, 0);   //时间间隔0.01s   
    timer_on(); 
    
	for (i = 0; i < TAPS+1; i++)
	{
        stateL[i] = 0; /* initialize state array */
        stateR[i] = 0; 
	}
	
	
    // Be in infinite loop and do nothing until done.
	while(1)
	{
		if(inputReady)
			handleCodecData(buffer_cntr);
	}
}

