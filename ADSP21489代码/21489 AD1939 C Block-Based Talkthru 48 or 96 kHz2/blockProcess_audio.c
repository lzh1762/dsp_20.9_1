///////////////////////////////////////////////////////////////////////////////////////
//                                                                                   //
// NAME:     blockProcess_audio.c (Block-based Talkthrough)                          //
// DATE:     02/06/10                                                                 //
// PURPOSE:  Process incoming AD1939 ADC data and prepare outgoing blocks for DAC.   //
//                                                                                   //
// USAGE:    This file contains the subroutines that float and fix the serial data,  //
//           and copy from the inputs to the outputs.                                //
//                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////

#include "ADDS_21489_EzKit.h"


#include <filter.h>  
#include <math.h>  
#define   NSECTIONS  1 
#define   NSAMPLES   256
#define   NSTATE     ((2*NSECTIONS) + 1)   
extern float pm  coeffs1[5*NSECTIONS];
extern float     state1[NSTATE];
extern float pm  coeffs2[5*NSECTIONS];
extern float     state2[NSTATE];

// Define a structure to represent buffers for all 12 floating-point data channels of the AD1939
typedef struct{
	float Rx_L1[NUM_SAMPLES];
	float Rx_R1[NUM_SAMPLES];
	float Rx_L2[NUM_SAMPLES];
	float Rx_R2[NUM_SAMPLES];

	float Tx_L1[NUM_SAMPLES];
	float Tx_R1[NUM_SAMPLES];
	float Tx_L2[NUM_SAMPLES];
	float Tx_R2[NUM_SAMPLES];
	float Tx_L3[NUM_SAMPLES];
	float Tx_R3[NUM_SAMPLES];
	float Tx_L4[NUM_SAMPLES];
	float Tx_R4[NUM_SAMPLES];
	
} ad1939_float_data;

// SPORT Ping/Pong Data buffers
extern int TxBlock_A0[];
extern int TxBlock_A1[];

extern int RxBlock_A0[];
extern int RxBlock_A1[];


//Pointer to the blocks
int *rx_block_pointer[2] = {RxBlock_A0, RxBlock_A1};
int *tx_block_pointer[2] = {TxBlock_A0, TxBlock_A1};


// Structures to hold floating point data for each AD1939
ad1939_float_data fBlockA;

void process_audioBlocks(void);

// Unoptimized function to convert the incoming fixed-point data to 32-bit floating-point format.
// This function assumes that the incoming fixed point data is in 1.31 format
void floatData(float *output, int *input, unsigned int instep, unsigned int length)
{
    int i;

    for(i = 0; i < length; i++)
    {
        output[i] = __builtin_conv_RtoF(input[instep*i]);
    }
}


// Unoptimized function to convert the outgoing floating-point data to 1.31 fixed-point format.
void fixData(int *output, float *input, unsigned int outstep, unsigned int length)
{
    int i;

    for(i = 0; i < length; i++)
    {
        output[outstep*i] = __builtin_conv_FtoR(input[i]);
    }
}


// Unoptimized function to copy from one floating-point buffer to another
void memcopy(float *input, float *output, unsigned int number)
{
    int i;
    
    for(i = 0; i < number; i++)
    {
        output[i] = input[i];
    }
}
void PEQProcess (float *in, float *out,float pm *coef,int samples,int sections) ///////////EQ
{
	// 对输入数据进行PEQ滤波处理
    biquad (in, out, coef, state1, samples, sections);
	
}

/////////////////////////////////////////////////////////////////////////////////////
// Audio Block Processing Algorithm for 4 IN x 8 OUT Audio System
	
// The inputs and outputs are held in a structure for the AD1939
// fBlockA holds stereo input (AIN) channels 0-3 and stereo output (AOUT) channels 0-7
	
// This function copys the data without any processing as follows
// AOUT1L <- AIN1L
// AOUT1R <- AIN1R
// AOUT2L <- AIN1L
// AOUT2R <- AIN1R

// AOUT3L <- AIN2L
// AOUT3R <- AIN2R
// AOUT4L <- AIN2L
// AOUT4R <- AIN2R
/////////////////////////////////////////////////////////////////////////////////////

void process_audioBlocks()
{
	float temp[256]=0.0;
	int i;
	//memcopy(fBlockA.Rx_L1, fBlockA.Tx_L1, NUM_SAMPLES);
	memcopy(fBlockA.Rx_R1, fBlockA.Tx_R1, NUM_SAMPLES);
	memcopy(fBlockA.Rx_L1, fBlockA.Tx_L2, NUM_SAMPLES);
	memcopy(fBlockA.Rx_R1, fBlockA.Tx_R2, NUM_SAMPLES);
	memcopy(fBlockA.Rx_L2, fBlockA.Tx_R3, NUM_SAMPLES);
	memcopy(fBlockA.Rx_R2, fBlockA.Tx_L3, NUM_SAMPLES);
	memcopy(fBlockA.Rx_L2, fBlockA.Tx_L4, NUM_SAMPLES);
	memcopy(fBlockA.Rx_R2, fBlockA.Tx_R4, NUM_SAMPLES);
        
	//PEQProcess(fBlockA.Rx_L1, fBlockA.Tx_L1 ,coeffs1,NSAMPLES,NSECTIONS);
	//PEQProcess(fBlockA.Rx_L1, temp ,coeffs2,NSAMPLES,NSECTIONS);     
	//for(i=0;i<256;i++) fBlockA.Tx_L1[i]=(fBlockA.Tx_L1[i]+temp[i])/2.0;
	
	
	PEQProcess(fBlockA.Rx_L1, temp ,coeffs1,NSAMPLES,NSECTIONS);
	PEQProcess(temp, fBlockA.Tx_L1 ,coeffs2,NSAMPLES,NSECTIONS);	
	}




/////////////////////////////////////////////////////////////////////////////////////
// This function handles the Codec data in the following 3 steps...
// 1. Converts all ADC data to 32-bit floating-point, and copies this 
//    from the current RX DMA buffer into fBlockA & fBlockB
// 2. Calls the audio processing function (processBlocks)
// 3. Converts all DAC to 1.31 fixed point, and copies this from 
//    fBlockA & fBlockB into the current TX DMA buffer
/////////////////////////////////////////////////////////////////////////////////////

void handleCodecData(unsigned int blockIndex)
{
    //Clear the Block Ready Semaphore
    inputReady = 0;

    //Set the Processing Active Semaphore before starting processing
    isProcessing = 1;

    // Float ADC data from AD1939
	floatData(fBlockA.Rx_L1, rx_block_pointer[blockIndex]+0, NUM_RX_SLOTS, NUM_SAMPLES);
	floatData(fBlockA.Rx_R1, rx_block_pointer[blockIndex]+1, NUM_RX_SLOTS, NUM_SAMPLES);
	floatData(fBlockA.Rx_L2, rx_block_pointer[blockIndex]+2, NUM_RX_SLOTS, NUM_SAMPLES);
	floatData(fBlockA.Rx_R2, rx_block_pointer[blockIndex]+3, NUM_RX_SLOTS, NUM_SAMPLES);

	// Place the audio processing algorithm here. 
	process_audioBlocks();

    // Fix DAC data for AD1939
	fixData(tx_block_pointer[blockIndex]+0, fBlockA.Tx_L1, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+1, fBlockA.Tx_R1, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+2, fBlockA.Tx_L2, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+3, fBlockA.Tx_R2, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+4, fBlockA.Tx_L3, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+5, fBlockA.Tx_R3, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+6, fBlockA.Tx_L4, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+7, fBlockA.Tx_R4, NUM_TX_SLOTS, NUM_SAMPLES);
	
    
    //Clear the Processing Active Semaphore after processing is complete
    isProcessing = 0;
}
