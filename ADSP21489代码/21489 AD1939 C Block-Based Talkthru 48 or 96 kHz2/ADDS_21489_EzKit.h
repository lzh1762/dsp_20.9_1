///////////////////////////////////////////////////////////////////////////////////////
//
//NAME:     ADDS_21489_EzKit.h
//DATE:     02/06/10
//PURPOSE:  Header file with definitions use in the C-based talkthrough examples
//
////////////////////////////////////////////////////////////////////////////////////////
#include <signal.h>
#include <cdef21489.h>
#include <def21489.h>

// Block Size per Audio Channel
#define NUM_SAMPLES 256

// Number of stereo channels
#define NUM_RX_SLOTS 4
#define NUM_TX_SLOTS 8

#define SPIB_MODE (CPHASE | CLKPL)
#define AD1939_CS DS0EN
//#define AD1939_CS DS1EN

#define SELECT_SPI_SLAVE(select) (*pSPIFLG &= ~(spiselect<<8))
#define DESELECT_SPI_SLAVE(select) (*pSPIFLG |= (spiselect<<8))

// Function prototypes for this talkthrough code

void initPLL_SDRAM(void);
void clearDAIpins(void);
void InitDAI(void);
void Init1939viaSPI(void);
void InitSPORT(void);

void ProccessingTooLong(void);
void TalkThroughISR(int);
void ClearSPORT(void);
void handleCodecData(unsigned int);

void SetupSPI1939(unsigned int);
void DisableSPI1939();
void Configure1939Register(unsigned char,unsigned char,unsigned char,unsigned int);
unsigned char Get1939Register(unsigned char,unsigned int);

void Delay(int i);

// Global Variables
extern int isProcessing;
extern int inputReady;
extern int buffer_cntr;

