/////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2009 - Analog Devices, Inc.                                               //
//                                                                                         //
// NAME:     main.c (sample-based Talkthrough)                                             //
// DATE:     02/06/10                                                                      //
// PURPOSE:  Function main() for AD1939/ADSP-21489 Talkthrough framework.                  //
//                                                                                         //
// USAGE:    This file contains the main routine calls functions to set up the talkthrough //
//           routine.                                                                      //
//                                                                                         //
//                                                                                         //
// Author(s):  VS., Analog Devices, Inc                                                    //
//                                                                                         //
//                                                                                         //
//                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////

#include "ADDS_21489_EzKit.h"

#include <filter.h>  
#include <math.h>  
#define   NSECTIONS  1 
#define   NSAMPLES   256
#define   NSTATE     ((2*NSECTIONS) + 1)   
#define   PI         3.1415926 
float     state1[NSTATE];
float pm  coeffs1[5*NSECTIONS];
float     state2[NSTATE];
float pm  coeffs2[5*NSECTIONS];

int CalulateEQParam(float pm *coeffs, int sections, int fs, float f, float q, float G);
void InitEQFilter(float dm *state, int nstate);
int       Fs;
float     fc1;
float     Q1;
float     Gain1;
int       i;

float     fc2;
float     Q2;
float     Gain2;

void	main()
{
		
	initPLL_SDRAM(); //Initialize the PLL and SDRAM controller
    // Initialize DAI because the SPORT and SPI signals
    // need to be routed
    InitDAI();
    // This function will configure the AD1939 codec on the 21489 EZ-KIT
    Init1939viaSPI();
    // Turn on SPORT0 TX and SPORT1 RX for Multichannel Operation
    InitSPORT();
	// Unmask SPORT1 RX ISR Interrupt 
    interrupt(SIG_SP1,TalkThroughISR);
    
    Fs       = 96000;
    
    fc1       = 3000;
    Q1        = 2;
    Gain1     = 15;
	CalulateEQParam(coeffs1, NSECTIONS,Fs, fc1, Q1, Gain1);
	InitEQFilter(state1, NSTATE);
	
    fc2       = 8000;
    Q2        = 2;
    Gain2     = -15;
	CalulateEQParam(coeffs2, NSECTIONS,Fs, fc2, Q2, Gain2);
	InitEQFilter(state2, NSTATE);
    
    // Be in infinite loop and do nothing until done.
	while(1)
	{
		if(inputReady)
			handleCodecData(buffer_cntr);
	}
}


int CalulateEQParam(float pm *coeffs, int sections, int fs, float f, float q, float G)
{ 
   
	// 计算滤波器系数的中间变量
    float Ag;
    float wf;
    float sinw;
    float cosw;
    float alpha;
    float temp1;
    float temp2;
    float temp3;
    float a0;
    float a1;
    float a2;
    float b0;
    float b1;
    float b2;
    float Fc;
    float Q1;
    float Gain;
    int Error = 0;  
    int k;    

        	
    Fc = f;
    if(Fc < 20)
    {
    	Fc = 20;
    	Error = 1;
    }
    if(Fc > 20000)
    {
    	Fc = 20000;
    	Error = 1;
    }
    	
    Q1 = q;
    if(Q1 < 0.5)
    {
    	Q1 = 0.5;
    	Error = 2;
    }
    if(Q1 > 32)
    {
    	Q1 = 32;
    	Error = 2;
    }
    	
    Gain = G;
    if(Gain < -20)
    {
    	Gain = -20;
    	Error = 3;
    }
    if(Gain > 20)
    {
    	Gain = 20;
    	Error = 3;
    }
    Ag    = pow(10,(Gain*0.025));  
    wf    = (2*PI/Fs)*Fc;
    sinw  = (float)sin(wf);
    cosw  = (float)cos(wf);
    alpha = (float)(sinw*0.5/Q1);
   
    temp1 = alpha*Ag;
    temp2 = alpha/Ag;  
  
  
    //计算系数b0, b1, b2
   	b0 = 1 + temp1;         //b0
    b1 = (-2) * cosw;       //b1
    b2 = 1 - temp1;         //b2

    //计算系数a0, a2.  a1不需计算， 因为a1=b1.
    a0 = 1 + temp2;        //a0
    a2 = 1 - temp2;        //a2
 
    //令a0=1, 得到修正后的各个系数,按以下顺序排列系数：-a2,   -a1,    b2,    b1,    b0 
    temp3=1/a0;  //  1/a0
    *(coeffs +  0) = -a2 * temp3;    //  保存修正后的-a2 
    *(coeffs +  1) = -b1 * temp3;    //  保存修正后的-a1。 此处采用b1进行计算是因为a1=b1.  
    *(coeffs +  2) =  b2 * temp3;    //  保存修正后的b2
    *(coeffs +  3) =  b1 * temp3;    //  保存修正后的b1
    *(coeffs +  4) =  b0 * temp3;    //  保存修正后的b0	 
    
    return Error;
}

void InitEQFilter(float dm *state, int nstate)
{
	int i;
	for (i = 0; i < nstate; i++)
	*(state + i) = 0; /* initialize state array */
}

