/////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2009 - Analog Devices, Inc.                                               //
//                                                                                         //
// NAME:     main.c (sample-based Talkthrough)                                             //
// DATE:     02/06/10                                                                      //
// PURPOSE:  Function main() for AD1939/ADSP-21489 Talkthrough framework.                  //
//                                                                                         //
// USAGE:    This file contains the main routine calls functions to set up the talkthrough //
//           routine.                                                                      //
//                                                                                         //
//                                                                                         //
// Author(s):  VS., Analog Devices, Inc                                                    //
//                                                                                         //
//                                                                                         //
//                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////

#include "ADDS_21489_EzKit.h"
#include "pbled_test.h"  //包含led 、按键的头文件
extern bool gb_sw8_pushed;   //为1时是按下了  需要手动清零
extern bool gb_sw9_pushed;   //全局变量，跟pbled_test.c中的 关联
extern bool gb_sw10_pushed;
extern bool gb_sw11_pushed;
bool led8_flag=1; 
bool led7_flag=1;   
bool led6_flag=1;	    
bool led5_flag=1; 

#include <stdio.h>
#include <def21489.h>
#include <21489.h>
#include <Cdef21489.h>
#include <signal.h>
#define SRUDEBUG  // Check SRU Routings for errors.
#include <SRU.h>

//int lost_flag=0;   //用于在解析命令初始化时  丢弃数据避免卡死

//3混响
#include "math.h"
#define COMB_N  4            // 梳状滤波器个数
int   delays[COMB_N]={2191,2971,3253,3307};// 梳状滤波器延时参数
float w[COMB_N]={0};    //参量
float kp[COMB_N]={0};   //参量
float Bp[COMB_N]={0};   //参量
float beta[COMB_N]={0};  //参量
int   ones[COMB_N]={1,1,1,1};   //参量

float rt60=0;/*混响时间*/
int   fs=96000;  /*采样频率*/
float alpha=0;

//1声向
#define TAPS 512
float stateL[TAPS+1];
float stateR[TAPS+1];
int i;

//0 基本的
//int bypass_flag=1;   //直通
int Vol_num=100;      //音量
int led_flag=0;
//EQ

#include <filter.h>  
#include <math.h>  
#define   NSECTIONS  1 
#define   NSAMPLES   256
#define   NSTATE     ((2*NSECTIONS) + 1)   
#define   PI         3.1415926 
float     state1[NSTATE];
float pm  coeffs1[5*NSECTIONS];
float     state2[NSTATE];
float pm  coeffs2[5*NSECTIONS];

int CalulateEQParam(float pm *coeffs, int sections, int fs, float f, float q, float G);
void InitEQFilter(float dm *state, int nstate);
int       Fs;
float     fc1;
float     Q1;
float     Gain1;
int       i;

float     fc2;
float     Q2;
float     Gain2;


//串口的
void initDPI(void);		//makes UART0 signal connections to DPI pins
void initUART(void);	//function for initializing the UART
void UARTisr();			//function for receive interrupt
void xmitUARTmessage(char*, int); // function to transmit the welcome message

int start_flag=0;      //接收到命令帧头  为1   其他为0
int order_num=0;      //接收到第几个数据
char order[30]={0};    //用于存放接收到的命令   不包含头尾

//有关命令的
int order1_flag=0;   //声向  开关标志
int order1_time=30;   //设置声向变化的间隔  越大越慢
int order1_dirnum=35;   //设置运动方向的  0-35代表0-350度

int order2_flag=0;    //EQ

int order3_flag=0;   //混响   开关   1开
int order3_time=15;  //混响时间的10倍 s
int order3_alpha=25;  //alpha的100倍  

//定时器
int timer_count = 1;
int timeout_falg = 0;  //用于定时时间到的标记 
void timer_isr (int sig)  //定时器中断服务函数
{
    //printf("Timer interrupt number %d\n",timer_count);
    timer_count++;
    if(timer_count>=order1_time){  //
    	timeout_falg++;
    	if(timeout_falg>=order1_dirnum){   // 0-35代表0-350度
    		timeout_falg=0;  //清空计数值
    	}
    	timer_count=0;  //清空计数值
    }
}

//extern void floatData(float *output, int *input, unsigned int instep, unsigned int length);

void	main()
{
	for (i = 0; i < TAPS+1; i++)
	{
        stateL[i] = 0; /* initialize state array */
        stateR[i] = 0; 
	}
		
	initPLL_SDRAM(); //Initialize the PLL and SDRAM controller
    // Initialize DAI because the SPORT and SPI signals
    // need to be routed
    InitDAI();
    // This function will configure the AD1939 codec on the 21489 EZ-KIT
    Init1939viaSPI();
    // Turn on SPORT0 TX and SPORT1 RX for Multichannel Operation
    InitSPORT();
	// Unmask SPORT1 RX ISR Interrupt 
    interrupt(SIG_SP1,TalkThroughISR);
    
    Init_LEDs();   //初始化 LED
    Init_PushButtons();  //初始化按键
    
    
//UART

	*pPICR2 &= ~(0x3E0); //Sets the UART0 receive interrupt to P13
	*pPICR2 |= (0x13<<5); 
	//initPLL_SDRAM();    炸了
	*pUART0LCR=0;
    *pUART0IER   = UARTRBFIE;    // enables UART0 receive interrupt
	interrupt(SIG_P13,UARTisr); 
	initDPI();
	initUART();   
	
	//混响
    rt60=1.0;/*混响时间*/
    alpha=0.25;
    for(i=0;i<COMB_N;i++)  	// 计算混响参数	
    {  
 	    w[i]=(-3*delays[i])/(rt60*fs);
        kp[i]=pow(10,w[i]);
        Bp[i]=ones[i]-2/(1+pow(kp[i],(1-1/alpha)));
        beta[i]=kp[i]*(1-Bp[i]);
    }  
    
    //定时器
	interrupts (SIG_TMZ, timer_isr);  //定时器中断设置
    timer_set(4000000, 0);   //时间间隔0.01s   
    timer_on(); 
    
//EQ
    Fs       = 96000;
    
    fc1       = 3000;
    Q1        = 2;
    Gain1     = 15;
	CalulateEQParam(coeffs1, NSECTIONS,Fs, fc1, Q1, Gain1);
	InitEQFilter(state1, NSTATE);
	
    fc2       = 8000;
    Q2        = 2;
    Gain2     = -15;
	CalulateEQParam(coeffs2, NSECTIONS,Fs, fc2, Q2, Gain2);
	InitEQFilter(state2, NSTATE);
	
	
    // Be in infinite loop and do nothing until done.
	while(1)
	{
		if(inputReady)
			handleCodecData(buffer_cntr);
			
		if(gb_sw8_pushed==1){  //如果按键8按下
			led8_flag=1-led8_flag;   //状态标志取反 
			ClearSet_LED(LED8, led8_flag);
			gb_sw8_pushed=0;  //清除按键8的状态
		}
		if(gb_sw9_pushed==1){  //如果按键9按下
			led7_flag=1-led7_flag;   //状态标志取反 
			ClearSet_LED(LED7, led7_flag);
			gb_sw9_pushed=0;  //清除按键8的状态
		}	
		if(gb_sw10_pushed==1){  //如果按键9按下
			led6_flag=1-led6_flag;   //状态标志取反 
			ClearSet_LED(LED6, led6_flag);
			gb_sw10_pushed=0;  //清除按键8的状态
		}
		if(gb_sw11_pushed==1){  //如果按键9按下
			led5_flag=1-led5_flag;   //状态标志取反 
			ClearSet_LED(LED5, led5_flag);
			gb_sw11_pushed=0;  //清除按键8的状态
		}
			
			
			
		if(start_flag==2){     //解析命令

		isProcessing=0;  //忽略一次的数据，避免卡死
		
      		xmitUARTmessage("\nget:",5);
      		xmitUARTmessage(order,order_num);
      		for(i=order_num;i<30;i++) order[order_num++]=0;
      		
      		
			if(order[0]=='0'){   //基本命令
			  //if(order[2]=='0'){
			  	 //bypass_flag=0;
			     //xmitUARTmessage("\n 1off!",7);
			  //}
			  if(order[2]=='1'){    //直通
			      //bypass_flag=1;
			      order1_flag=0;
			      order2_flag=0;
			      order3_flag=0;
			     //xmitUARTmessage("\n 1on!",6);
			  } 
			  Vol_num=(order[4]-'0')*100+(order[5]-'0')*10+order[6]-'0';
			  if(order[8]=='0'){
			  	 led_flag=1;
			  	 ClearSet_LED(LED1, led_flag);
			  	 ClearSet_LED(LED2, led_flag);
			  	 ClearSet_LED(LED3, led_flag);
			  	 ClearSet_LED(LED4, led_flag);
			  	 
			     //xmitUARTmessage("\n 1off!",7);
			  }
			  if(order[8]=='1'){
			      led_flag=0;
			  	 ClearSet_LED(LED1, led_flag);
			  	 ClearSet_LED(LED2, led_flag);
			  	 ClearSet_LED(LED3, led_flag);
			  	 ClearSet_LED(LED4, led_flag);
			     //xmitUARTmessage("\n 1on!",6);
			  }  
			}  
      	

			if(order[0]=='1'){   //声向  的命令
				for (i = 0; i < TAPS+1; i++){   //清空 初始化
    			    stateL[i] = 0; /* initialize state array */
    			    stateR[i] = 0; 
				}

			  if(order[2]=='0'){
			  	 order1_flag=0;
			     //xmitUARTmessage("\n 1off!",7);
			  }
			  if(order[2]=='1'){
			      order1_flag=1;
			     //xmitUARTmessage("\n 1on!",6);
			  } 
			  order1_time=(order[4]-'0')*10+order[5]-'0';
			  order1_dirnum=(order[7]-'0')*10+order[8]-'0';
			}  
			if(order[0]=='2'){   //EQ
			  if(order[2]=='0'){
			  	 order2_flag=0;
			  }
			  if(order[2]=='1'){
			      order2_flag=1;
			  }	
    		  fc1=(order[4]-'0')*10000+(order[5]-'0')*1000+(order[6]-'0')*100+(order[7]-'0')*10+(order[8]-'0');
			  Q1=order[10]-'0';
			  Gain1=(order[12]-'0')*10+order[13]-'0'-50;   //注意  -50
			  
    		  fc2=(order[15]-'0')*10000+(order[16]-'0')*1000+(order[17]-'0')*100+(order[18]-'0')*10+(order[19]-'0');
			  Q2=order[21]-'0';
			  Gain2=(order[23]-'0')*10+order[24]-'0'-50;   //注意  -50		  
			  
	CalulateEQParam(coeffs1, NSECTIONS,Fs, fc1, Q1, Gain1);
	InitEQFilter(state1, NSTATE);
	CalulateEQParam(coeffs2, NSECTIONS,Fs, fc2, Q2, Gain2);
	InitEQFilter(state2, NSTATE);
			  
			  
			
			}
			if(order[0]=='3'){     //混响  的命令
			  if(order[2]=='0'){
			  	 order3_flag=0;
			     //xmitUARTmessage("\n 1off!",7);
			  }
			  if(order[2]=='1'){
			      order3_flag=1;
			     //xmitUARTmessage("\n 1on!",6);
			  }
    		  rt60=(float)((order[4]-'0')*10+order[5]-'0')/10.0;
    		  alpha=(float)((order[7]-'0')*10+order[8]-'0')/100.0;

    	     for(i=0;i<COMB_N;i++){  	// 计算混响参数	   太久
 				 w[i]=(-3*delays[i])/(rt60*fs);
   			     kp[i]=pow(10,w[i]);
   			     Bp[i]=ones[i]-2/(1+pow(kp[i],(1-1/alpha)));
   			     beta[i]=kp[i]*(1-Bp[i]);
   			  } 
	
    
         
    
			}
			start_flag=0;    //只解析一次
		
		}
		
	}
}

//有关EQ

int CalulateEQParam(float pm *coeffs, int sections, int fs, float f, float q, float G)
{ 
   
	// 计算滤波器系数的中间变量
    float Ag;
    float wf;
    float sinw;
    float cosw;
    float alpha;
    float temp1;
    float temp2;
    float temp3;
    float a0;
    float a1;
    float a2;
    float b0;
    float b1;
    float b2;
    float Fc;
    float Q1;
    float Gain;
    int Error = 0;  
    int k;    

        	
    Fc = f;
    if(Fc < 20)
    {
    	Fc = 20;
    	Error = 1;
    }
    if(Fc > 20000)
    {
    	Fc = 20000;
    	Error = 1;
    }
    	
    Q1 = q;
    if(Q1 < 0.5)
    {
    	Q1 = 0.5;
    	Error = 2;
    }
    if(Q1 > 32)
    {
    	Q1 = 32;
    	Error = 2;
    }
    	
    Gain = G;
    if(Gain < -20)
    {
    	Gain = -20;
    	Error = 3;
    }
    if(Gain > 20)
    {
    	Gain = 20;
    	Error = 3;
    }
    Ag    = pow(10,(Gain*0.025));  
    wf    = (2*PI/Fs)*Fc;
    sinw  = (float)sin(wf);
    cosw  = (float)cos(wf);
    alpha = (float)(sinw*0.5/Q1);
   
    temp1 = alpha*Ag;
    temp2 = alpha/Ag;  
  
  
    //计算系数b0, b1, b2
   	b0 = 1 + temp1;         //b0
    b1 = (-2) * cosw;       //b1
    b2 = 1 - temp1;         //b2

    //计算系数a0, a2.  a1不需计算， 因为a1=b1.
    a0 = 1 + temp2;        //a0
    a2 = 1 - temp2;        //a2
 
    //令a0=1, 得到修正后的各个系数,按以下顺序排列系数：-a2,   -a1,    b2,    b1,    b0 
    temp3=1/a0;  //  1/a0
    *(coeffs +  0) = -a2 * temp3;    //  保存修正后的-a2 
    *(coeffs +  1) = -b1 * temp3;    //  保存修正后的-a1。 此处采用b1进行计算是因为a1=b1.  
    *(coeffs +  2) =  b2 * temp3;    //  保存修正后的b2
    *(coeffs +  3) =  b1 * temp3;    //  保存修正后的b1
    *(coeffs +  4) =  b0 * temp3;    //  保存修正后的b0	 
    
    return Error;
}

void InitEQFilter(float dm *state, int nstate)
{
	int i;
	for (i = 0; i < nstate; i++)
	*(state + i) = 0; /* initialize state array */
}















//UART相关函数--------------------
void initDPI()
{
	
  SRU2(UART0_TX_O,DPI_PB09_I); // UART transmit signal is connected to DPI pin 9
  SRU2(HIGH,DPI_PBEN09_I);
  SRU2(DPI_PB10_O,UART0_RX_I); // connect the pin buffer output signal to the UART0 receive
  SRU2(LOW,DPI_PB10_I);
  SRU2(LOW,DPI_PBEN10_I);      // disables DPI pin10 as input
}
void initUART()
{
	/* Sets the Baud rate for UART0 */	
	*pUART0LCR = UARTDLAB;  //enables access to Divisor register to set bauda rate
	//*pUART0DLL = 0x8B;      //0x28b = 651 for divisor value and gives a baud rate of 19200 for peripheral clock of 200MHz
    //*pUART0DLH = 0x02; 
	*pUART0DLL = 0x6C;      //0x 6C = 108 for 115200;  0x28b = 651 for divisor value and gives a baud rate of 19200 for peripheral clock of 200MHz
    *pUART0DLH = 0x00; 
    
    /* Configures UART0 LCR */ 
    *pUART0LCR = UARTWLS8| 				// word length 8
                 UARTPEN| 				// parity enable ODD parity
                 UARTSTB ; 				// Two stop bits
               
               
    *pUART0RXCTL = UARTEN;       //enables UART0 in receive mode

    *pUART0TXCTL = UARTEN;       //enables UART0 in core driven mode
  
    //xmitUARTmessage(welcomemessage,sizeof(welcomemessage));   
}
void xmitUARTmessage(char*xmit, int SIZE)
{
	int i;
	
	/* loop to transmit source array in core driven mode */   
  for (i=0; i<SIZE; i++)
  {
    // Wait for the UART transmitter to be ready
    do { 
    	;}
    while ((*pUART0LSR & UARTTHRE) == 0);
    
    //Transmit a byte
    *pUART0THR = xmit[i]; 
   }
  
/* poll to ensure UART has completed the transfer */
  while ((*pUART0LSR & UARTTEMT) == 0)
   {;
   } 

}
void UARTisr()
{
 int value;
 
 value = *pUART0RBR;
 
 /* echoes back the value on to the hyperterminal screen*/
  while ((*pUART0LSR & UARTTHRE) == 0)
  {;
  }
  
  
  if(value=='*'){
      start_flag=2;
  }
  
  if(start_flag==1){
  	order[order_num++]=value;
  }
  
  if(value=='#'){
  	start_flag=1;   
  	order_num=0;	
  }

  
  
  
  /**pUART0THR = value;
  while ((*pUART0LSR & UARTTEMT) == 0)
   {;
   }*/ 
   
}


