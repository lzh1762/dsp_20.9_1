///////////////////////////////////////////////////////////////////////////////////////
//NAME:     SPORT1_isr.c (Block-based Talkthrough)
//DATE:     02/06/10
//PURPOSE:  Talkthrough framework for sending and receiving samples to the AD1939.
//
//USAGE:    This file contains SPORT1 Interrupt Service Routine. Four buffers are used
//          for this example: Two input buffers, and two output buffers.
///////////////////////////////////////////////////////////////////////////////////////
/*
   Here is the mapping between the SPORTS and the ADCs/DACs
   For AD1939
   ADC1 -> DSP : SPORT1A : TDM Channel 0,1
   ADC2 -> DSP : SPORT1A : TDM Channel 2,3
   DSP -> DAC1 : SPORT0A : TDM Channel 0,1
   DSP -> DAC2 : SPORT0A : TDM Channel 2,3
   DSP -> DAC3 : SPORT0A : TDM Channel 4,5
   DSP -> DAC4 : SPORT0A : TDM Channel 6,7   
*/

#include "ADDS_21489_EzKit.h"
#include <sru.h>

// Counter to choose which buffer to process
int buffer_cntr = 1;
// Semaphore to indicate to main that a block is ready for processing
int inputReady = 0;
// Semaphore to indicate to the isr that the processing has not completed before the
// buffer will be overwritten.
int isProcessing = 0;

//If the processing takes too long, the program will be stuck in this infinite loop.
void ProcessingTooLong(void)
{
    while(1);
}

int run_times=0;
void TalkThroughISR(int sig_int)
{
    int i;
    

    if(isProcessing){
    	run_times++;
    	if(run_times>10)  //连续10次没有处理的来数据就  卡死
            ProcessingTooLong(); 
    }
    else{
        run_times=0;
    }
    //Increment the block pointer
    buffer_cntr++;
    buffer_cntr %= 2;
    inputReady = 1;

}
