-------------------------------------------------------------------------------
Example: C Block-based talkthrough (using 48kHz or 96 kHz sample rates)
Target: ADSP-21489
Date: February 23,2010
Tools: VisualDSP++ 5.0 ADSP-21479 and 21489 EZ-Boards, VisualDSP++5.0 Update8, and above
Hardware: ADSP-21489 EZ-board
-------------------------------------------------------------------------------
This project contains a talkthrough example using the onboard AD1939 to acquire
and output an audio stream. The digital audio data is available for processing
in the file SPORT1_isr.c. The block size is 256 samples per audio channel.

The AD1939 can be set up for 48/96/192 Khz Sampling rate.  The ADC is connected 
to SPORT 0A. DAC1 is connected to SPORT 1A, DAC2 to SPORT 1A, DAC3 to SPORT1A, 
and DAC4 (Headphone output) to SPORT1A. All channels of the codec are accessed in
TDM mode. See initSRU.c for the details of which DAI pins are used to access the
codec. 

-------------------------------------------------------------------------------
Instructions: Apply an input signal to J4/J5 bottom rows on the ADSP-21489 
              EZ-board, and attach an output device to the selected middle 
              or top row channels of J4/J5. Input to ADC IN 1  gives output on 
              DAC OUT1 and DAC OUT2 and Input to ADC IN2 gives output on
              DAC OUT3 and DAC OUT4.
              
Input/Output combinations:
              J4 bottom row RCA connectors - ADC IN 1 Left/Right Channels
	          - J4 middle row RCA connectors - DAC OUT 1 Left/Right Channel
                  - J5 middle row RCA connectors - DAC OUT 2 Left/Right Channels
              
              J5 bottom row RCA connectors - ADC IN 2 Left/Right Channels
                  - J4 top row RCA connectors - DAC OUT 3 Left/Right Channels
                  - J5 top row RCA connectors - DAC OUT 4 Left/Right Channels
                  - Headphone jack (H)

                  
|  J8 |      J4       |     J5        |
---------------------------------------
|     | *O3l* | *O3r* | *O4l* | *O4r* |
|     | +O1l+ | +O1r+ | +O2l+ | +O2r+ |
| *H* | +I1l+ | +I1r+ | *I2l* | *I2r* |
---------------------------------------

                  
Switch settings for rev 0.2 EZ-Board:
SW1 		ON, ON, ON, ON, ON, ON, ON, ON
SW2 		ON, ON, ON, ON, OFF, OFF, ON, ON
SW3 		ALL ON
SW15-18 	ON, OFF, ON, OFF, ON, OFF
SW23 		ALL ON
SW24 		ALL OFF
SW25 		ALL OFF

NOTE:  If the application is halted within the VDSP system, the executable should 
       be reloaded to the target to ensure proper operation.
       
-------------------------------------------------------------------------------
Source Files contained in this directory:
21489 AD1939 C Block-Based Talkthru 48 or 96 kHz.dpj   VisualDSP project file
ad1939.h                Macro Definitions for AD1939 registers
ADDS_21489_EzKit.h      Includes and external declarations used for all files
blockProcess_audio.c    Process the audio data in the current block
init1939viaSPI.c        ADSP-21489 source - AD1939 SPI Control Port Subroutines
init_PLL_SDRAM.c        Configures core for 400 MHz and enables SDRAM memory
initSRU.c               Set up the DAI pins and SRU to connect to the AD1939
main.c                  Main section to call setup routines
initSPORT01_TDM_mode.c  Initialize the SPORT DMA to communicate with the AD1939
SPORT1_isr.c             Process SPORT 1 interrupts
-------------------------------------------------------------------------------
Dependencies contained in VisualDSP++ default include path:
def21489.h              Header file with generic definitions for ADSP-21489
SRU.h                   Header file with SRU definitions and Macros

*******************************************************************************
Analog Devices, Inc.
DSP Division
Three Technology Way
Norwood, MA 02062

(c) 2010 Analog Devices, Inc.  All rights reserved.
*******************************************************************************
