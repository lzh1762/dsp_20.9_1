///////////////////////////////////////////////////////////////////////////////////////
//                                                                                   //
// NAME:     blockProcess_audio.c (Block-based Talkthrough)                          //
// DATE:     02/06/10                                                                 //
// PURPOSE:  Process incoming AD1939 ADC data and prepare outgoing blocks for DAC.   //
//                                                                                   //
// USAGE:    This file contains the subroutines that float and fix the serial data,  //
//           and copy from the inputs to the outputs.                                //
//                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////

#include "ADDS_21489_EzKit.h"

#include <filter.h>
#define TAPS 512
#define SAMPLES 256
extern int timeout_falg; 
int i=0;

//对应命令控制的变量
extern int order1_flag;   //声向  开关标志
extern int order2_flag;   //EQ
extern int order3_flag;    //混响   开关   1开
//extern int bypass_flag;   //直通
extern int Vol_num;      //音量


float pm hL[TAPS]=
{
    #include"hL.dat"  
};
float pm hR[TAPS]=
{
    //#include"/dat0R/H90cR.dat"
    #include"hR.dat"
};

#pragma default_section(CODE,"seg_sdram")   //外部存储
//float hL36[36][TAPS]=  //这个才是对的
float hL36[1][TAPS]=    //改成这个 让存储空间给  混响的
{
    #include"data36L.dat"   
};
//float hR36[36][TAPS]=  //这个才是对的
float hR36[1][TAPS]=   //改成这个 让存储空间给  混响的
{
    #include"data36R.dat"   
};
 
#pragma default_section(CODE,"seg_ext_code") 

extern float stateL[TAPS+1];
extern float stateR[TAPS+1];


//EQ
#include <math.h>  
#define   NSECTIONS  1 
#define   NSAMPLES   256
#define   NSTATE     ((2*NSECTIONS) + 1)   
extern float pm  coeffs1[5*NSECTIONS];
extern float     state1[NSTATE];
extern float pm  coeffs2[5*NSECTIONS];
extern float     state2[NSTATE];

// Define a structure to represent buffers for all 12 floating-point data channels of the AD1939
typedef struct{
	float Rx_L1[NUM_SAMPLES];
	float Rx_R1[NUM_SAMPLES];
	float Rx_L2[NUM_SAMPLES];
	float Rx_R2[NUM_SAMPLES];

	float Tx_L1[NUM_SAMPLES];
	float Tx_R1[NUM_SAMPLES];
	float Tx_L2[NUM_SAMPLES];
	float Tx_R2[NUM_SAMPLES];
	float Tx_L3[NUM_SAMPLES];
	float Tx_R3[NUM_SAMPLES];
	float Tx_L4[NUM_SAMPLES];
	float Tx_R4[NUM_SAMPLES];
	
} ad1939_float_data;

// SPORT Ping/Pong Data buffers
extern int TxBlock_A0[];
extern int TxBlock_A1[];

extern int RxBlock_A0[];
extern int RxBlock_A1[];


//Pointer to the blocks
int *rx_block_pointer[2] = {RxBlock_A0, RxBlock_A1};
int *tx_block_pointer[2] = {TxBlock_A0, TxBlock_A1};


// Structures to hold floating point data for each AD1939
ad1939_float_data fBlockA;

void process_audioBlocks(void);

// Unoptimized function to convert the incoming fixed-point data to 32-bit floating-point format.
// This function assumes that the incoming fixed point data is in 1.31 format
void floatData(float *output, int *input, unsigned int instep, unsigned int length)
{
    int i;

    for(i = 0; i < length; i++)
    {
        output[i] = __builtin_conv_RtoF(input[instep*i]);
    }
}


// Unoptimized function to convert the outgoing floating-point data to 1.31 fixed-point format.
void fixData(int *output, float *input, unsigned int outstep, unsigned int length)
{
    int i;

    for(i = 0; i < length; i++)
    {
        output[outstep*i] = __builtin_conv_FtoR(input[i]);
    }
}


// Unoptimized function to copy from one floating-point buffer to another
void memcopy(float *input, float *output, unsigned int number)
{
    int i;
    
    for(i = 0; i < number; i++)
    {
        output[i] = input[i];
    }
}
//混响的
//#pragma default_section(CODE,"seg_sdram")   //外部存储
#define COMB_N  4            // 梳状滤波器个数
#define AP_N    2            // 全通滤波器个数
//#define NUM_SAMPLES    256   // 一帧输入数据长度
#define MAX_COMB_DELAY 3307  // 梳状滤波器最大延时点数
#define MAX_AP_DELAY   713   // 全通滤波器最大延时点数
 
extern int   delays[COMB_N];// 梳状滤波器延时参数
extern float Bp[COMB_N];
extern float beta[COMB_N];
int   apdelays[AP_N]={441,713}; //参量
float apgains[AP_N]={0.5,0.5};  //参量

float lp_d1[COMB_N]={0};           //由于用short数组，但是delaylines数组得到的数据是float类型的，所以不改成float类型，delaylines得到的数据全部为0
float delaylines1[COMB_N][MAX_COMB_DELAY]={{0},{0},{0},{0}};  //这个对的
float apdelaylines1[MAX_AP_DELAY]={{0},{0}};  //对的
//float delaylines1[4][10]={{0},{0},{0},{0}};
//float apdelaylines1[10]={{0},{0}};

int   dl_p1[COMB_N]={1,1,1,1};
float temp_out1[COMB_N]={0};
int   ap_dlp1=1;
float ap_out1=0;
float y1[NUM_SAMPLES]={0}; 
/*
float lp_d2[COMB_N]={0};           //由于用short数组，但是delaylines数组得到的数据是float类型的，所以不改成float类型，delaylines得到的数据全部为0
float delaylines2[COMB_N][MAX_COMB_DELAY]={{0},{0},{0},{0}};
float apdelaylines2[MAX_AP_DELAY]={{0},{0}};
int   dl_p2[COMB_N]={1,1,1,1};
float temp_out2[COMB_N]={0};
int   ap_dlp2=1;
float ap_out2=0;
float y2[NUM_SAMPLES]={0}; */

void RiittaSchroeder1(float *input, float *output,unsigned int m)  //1的
{
	int i,j;
    for(i=0;i<m;i++)
    {
        for(j=0;j<COMB_N;j++)
        {
            y1[i] += temp_out1[j];
            delaylines1[j][dl_p1[j]-1] = input[i] + temp_out1[j];
            dl_p1[j] = ((dl_p1[j]+1) % delays[j]) + 1;
            temp_out1[j] = delaylines1[j][dl_p1[j]-1]+Bp[j]*lp_d1[j];   
            temp_out1[j] *= beta[j];
            lp_d1[j]=delaylines1[j][dl_p1[j]-1];
        }
        apdelaylines1[ap_dlp1-1] = y1[i] + ap_out1*apgains[1];
        ap_dlp1 = (ap_dlp1+1) % apdelays[1] + 1;
        ap_out1 = apdelaylines1[ap_dlp1] - apgains[1]*y1[i];
        y1[i] = ap_out1 + input[i];
        output[i] = y1[i];
        y1[i]=0;
    }
}
/*
void RiittaSchroeder2(float *input, float *output,unsigned int m)  //2的
{
	int i,j;
    for(i=0;i<m;i++)
    {
        for(j=0;j<COMB_N;j++)
        {
            y2[i] += temp_out2[j];
            delaylines2[j][dl_p2[j]-1] = input[i] + temp_out2[j];
            dl_p2[j] = ((dl_p2[j]+1) % delays[j]) + 1;
            temp_out2[j] = delaylines2[j][dl_p2[j]-1]+Bp[j]*lp_d2[j];   
            temp_out2[j] *= beta[j];
            lp_d2[j]=delaylines2[j][dl_p2[j]-1];
        }
        apdelaylines2[ap_dlp2-1] = y2[i] + ap_out2*apgains[1];
        ap_dlp2 = (ap_dlp2+1) % apdelays[1] + 1;
        ap_out2 = apdelaylines2[ap_dlp2] - apgains[1]*y2[i];
        y2[i] = ap_out2 + input[i];
        output[i] = y2[i];
        y2[i]=0;
    }
}*/

void PEQProcess (float *in, float *out,float pm *coef,int samples,int sections) ///////////EQ
{
	// 对输入数据进行PEQ滤波处理
    biquad (in, out, coef, state1, samples, sections);
	
}

/////////////////////////////////////////////////////////////////////////////////////
// Audio Block Processing Algorithm for 4 IN x 8 OUT Audio System
	
// The inputs and outputs are held in a structure for the AD1939
// fBlockA holds stereo input (AIN) channels 0-3 and stereo output (AOUT) channels 0-7
	
// This function copys the data without any processing as follows
// AOUT1L <- AIN1L
// AOUT1R <- AIN1R
// AOUT2L <- AIN1L
// AOUT2R <- AIN1R

// AOUT3L <- AIN2L
// AOUT3R <- AIN2R
// AOUT4L <- AIN2L
// AOUT4R <- AIN2R
/////////////////////////////////////////////////////////////////////////////////////

void process_audioBlocks()
{

	//memcopy(fBlockA.Rx_L1, fBlockA.Tx_L1, NUM_SAMPLES);
	//memcopy(fBlockA.Rx_R1, fBlockA.Tx_R1, NUM_SAMPLES);
	memcopy(fBlockA.Rx_L1, fBlockA.Tx_L2, NUM_SAMPLES);
	memcopy(fBlockA.Rx_R1, fBlockA.Tx_R2, NUM_SAMPLES);
	//memcopy(fBlockA.Rx_L2, fBlockA.Tx_R3, NUM_SAMPLES);
	//memcopy(fBlockA.Rx_R2, fBlockA.Tx_L3, NUM_SAMPLES);
	//memcopy(fBlockA.Rx_L2, fBlockA.Tx_L4, NUM_SAMPLES);
	//memcopy(fBlockA.Rx_R2, fBlockA.Tx_R4, NUM_SAMPLES);
for(i=0;i<256;i++){
	fBlockA.Rx_L1[i]=fBlockA.Rx_L1[i]*Vol_num/100.0;
	fBlockA.Rx_R1[i]=fBlockA.Rx_R1[i]*Vol_num/100.0;
}
	
	
  if(order1_flag==1){    //声向
    for(i=0;i<512;i++){   //复制
    	hL[i]=hL36[timeout_falg][i];
    }
    for(i=0;i<512;i++){   //复制
    	hR[i]=hR36[timeout_falg][i];
    }  
    fir (fBlockA.Rx_L1, fBlockA.Tx_L1, hL, stateL, SAMPLES, TAPS);
    fir (fBlockA.Rx_R1, fBlockA.Tx_R1, hR, stateR, SAMPLES, TAPS);
  }
  
    	float temp[256]={0.0};
  if(order2_flag==1){   //EQ

	PEQProcess(fBlockA.Rx_L1, temp ,coeffs1,NSAMPLES,NSECTIONS);
	PEQProcess(temp, fBlockA.Tx_L1 ,coeffs2,NSAMPLES,NSECTIONS);
	memcopy(fBlockA.Tx_L1, fBlockA.Tx_R1, NUM_SAMPLES);		
  }  
  if(order3_flag==1){  //混响
	RiittaSchroeder1(fBlockA.Rx_L1, fBlockA.Tx_L1,NUM_SAMPLES);   //有中间量，不可通用  左声道为主
    //RiittaSchroeder2(fBlockA.Rx_R1, fBlockA.Tx_R1,NUM_SAMPLES);    
	memcopy(fBlockA.Tx_L1, fBlockA.Tx_R1, NUM_SAMPLES);    //两个混响 运算太久  就用一个，复制过来  
  }
  
  
    
  if((order1_flag==0)&&(order2_flag==0)&&(order3_flag==0)){      //直通  添加东西
  	memcopy(fBlockA.Rx_L1, fBlockA.Tx_L1, NUM_SAMPLES);
	memcopy(fBlockA.Rx_R1, fBlockA.Tx_R1, NUM_SAMPLES);
  }
	
}




/////////////////////////////////////////////////////////////////////////////////////
// This function handles the Codec data in the following 3 steps...
// 1. Converts all ADC data to 32-bit floating-point, and copies this 
//    from the current RX DMA buffer into fBlockA & fBlockB
// 2. Calls the audio processing function (processBlocks)
// 3. Converts all DAC to 1.31 fixed point, and copies this from 
//    fBlockA & fBlockB into the current TX DMA buffer
/////////////////////////////////////////////////////////////////////////////////////

void handleCodecData(unsigned int blockIndex)
{
    //Clear the Block Ready Semaphore
    inputReady = 0;

    //Set the Processing Active Semaphore before starting processing
    isProcessing = 1;

    // Float ADC data from AD1939
	floatData(fBlockA.Rx_L1, rx_block_pointer[blockIndex]+0, NUM_RX_SLOTS, NUM_SAMPLES);
	floatData(fBlockA.Rx_R1, rx_block_pointer[blockIndex]+1, NUM_RX_SLOTS, NUM_SAMPLES);
	floatData(fBlockA.Rx_L2, rx_block_pointer[blockIndex]+2, NUM_RX_SLOTS, NUM_SAMPLES);
	floatData(fBlockA.Rx_R2, rx_block_pointer[blockIndex]+3, NUM_RX_SLOTS, NUM_SAMPLES);

	// Place the audio processing algorithm here. 
	process_audioBlocks();

    // Fix DAC data for AD1939
	fixData(tx_block_pointer[blockIndex]+0, fBlockA.Tx_L1, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+1, fBlockA.Tx_R1, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+2, fBlockA.Tx_L2, NUM_TX_SLOTS, NUM_SAMPLES);
	fixData(tx_block_pointer[blockIndex]+3, fBlockA.Tx_R2, NUM_TX_SLOTS, NUM_SAMPLES);
	//fixData(tx_block_pointer[blockIndex]+4, fBlockA.Tx_L3, NUM_TX_SLOTS, NUM_SAMPLES);
	//fixData(tx_block_pointer[blockIndex]+5, fBlockA.Tx_R3, NUM_TX_SLOTS, NUM_SAMPLES);
	//fixData(tx_block_pointer[blockIndex]+6, fBlockA.Tx_L4, NUM_TX_SLOTS, NUM_SAMPLES);
	//fixData(tx_block_pointer[blockIndex]+7, fBlockA.Tx_R4, NUM_TX_SLOTS, NUM_SAMPLES);
	
    
    //Clear the Processing Active Semaphore after processing is complete
    isProcessing = 0;
}
