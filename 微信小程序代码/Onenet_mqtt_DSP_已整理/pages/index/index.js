//get和发cmd的实现，三种状态的显示（小程序与onenet）
//有onload，分立按键，led控制灯开与关 和key
//发送数据、接收全部数据 封装成函数、小程序与onenet反馈成浮窗，留下终端显示和数据
//可以设置每3秒获取一次数据、增加slider、swatch等，与onenet的应用功能基本相同

//获取应用实例index.js
const app = getApp()


//mqtt的下发在线命令 无需应答  结尾“，”
function Sendcmd(cmd, that) {
  if(that.data.isonline == "离线"){ wx.showToast({
      title: "设备离线！",
      icon: "loading",
      duration: 2500
    })
    return;
  }
  console.log("发送" + cmd +"命令：")
  wx.request({
    url: "https://api.heclouds.com/cmds?device_id="+that.data.inputID,
    method: 'POST',
    header: {      "api-key": that.data.Apikey    },
    data: cmd,
    success(res) {
      console.log(res.data) //打印返回data
      if (res.data.errno == 0) { //如果post返回  正常
        
        wx.showToast({
          title: cmd + "成功",
          icon: "success",  
          // icon: "loading",
          duration: 500  //默认1500
        })
      } else{ 
        wx.showToast({
          title: cmd + "出错" + res.data.errno,
          // icon: "success",
          icon: "loading",
          duration: 2500  
        })
      }
    },
    fail(res) { //没有返回数据
      // that.setData({
      //   wc_state: stream_id + stream_value +"失败",
      // })
      wx.showToast({
        title: cmd + "失败",
        // icon: "success",
        icon: "loading",
        duration: 2500
      })
    }
  })
}
function Getdata_refresh(that){ //获取数据并更新  正常时无提示，错误时浮窗状态
  console.log("Getdata_refresh  获取状态、数据+更新")
  wx.request({     //获取状态
    url: 'https://api.heclouds.com/devices/'+that.data.inputID,
    header: {
      'api-key': that.data.Apikey // 填写自己的
    },
    method: 'get',
    dataType: 'json',
    success(res) {
      console.log(res.data.data)
      if (res.data.errno == 0) { //如果GET返回无错误
        console.log("获取在线状态 成功")

        if (res.data.data.online == 0) that.setData({ 
          isonline: "离线",     
          isonline_color: "red",
        })
        else that.setData({
          isonline: "在线",     
          isonline_color: "green",
        })

      } else{ wx.showToast({
        title: "get在线？出错" + res.data.errno,
        icon: "loading",
        duration: 2500
      })
      that.setData({ 
        isonline: "离线",     
        isonline_color: "red",
      })
      }
    },
    fail(res) {
      wx.showToast({
        title: "get在线？失败",
        icon: "loading",
        duration: 2500
      })
      that.setData({ 
        isonline: "离线",     
        isonline_color: "red",
      })
    }
  });
  wx.request({     //获取数据+更新
    url: 'https://api.heclouds.com/devices/'+that.data.inputID+'/datastreams',
    header: {
      'api-key': that.data.Apikey // 填写自己的
    },
    method: 'get',
    dataType: 'json',
    success(res) {
      console.log("获取全部数据：")
      console.log(res.data.data) 
      if (res.data.errno == 0) { //如果GET返回无错误

        console.log("获取全部数据 成功")
        that.setData({         //注意数据顺序
          // esp_state: JSON.stringify(res.data.data[0].current_value),     //json转为string
          // esp_humi: res.data.data[1].current_value,
          // esp_temp: res.data.data[2].current_value,
          function0_state: res.data.data[0].current_value,
          function1_state: res.data.data[1].current_value,
          function2_state: res.data.data[4].current_value,
          function3_state: res.data.data[3].current_value,
        }) 
      } else wx.showToast({
        title: "get全数据 出错" + res.data.errno,
        icon: "loading",
        duration: 2500
      })
    },
    fail(res) {
      wx.showToast({
        title: "get全数据 失败",
        icon: "loading",
        duration: 2500
      })
    }
  });

}

var get_data_timer;  
Page({
  data: { //页面涉及的数据：

    isonline: "离线",     //终端是否在线
    isonline_color: "red",
    inputID: "621408218",    //设备ID
    Apikey: "xxx",  //onenet 的Apikey 为安全这里用xxx代替
    inputcmd: 'Input the cmds',

    Rslider_value: 200,
    Gslider_value: 255,
    Bslider_value: 200,

    function0_state: "初始化ing",
    function1_state: "初始化ing",
    function2_state: "初始化ing",
    function3_state: "初始化ing",
    Vol_value: 100,

    time_value:30,
    range_value:35,

    fc1_value:3000,
    Q1_value:2,
    G1_value:15,
    fc2_value:8000,
    Q2_value:2,
    G2_value:15,

    rt60_value:15,
    alpha_value:25,
    esp_state: "初始化ing",
  },

  onLoad: function() { //页面载入时执行
    var that = this;
    Getdata_refresh(that)
  },
  onShow: function() {
    var that = this;
    get_data_timer = setInterval(function() { //定时器  每隔ms执行一次
      Getdata_refresh(that)
    }, 3000)
  },
  onHide: function() {
    clearTimeout(get_data_timer)
    // console.log('index:onHide')
  },
  onUnload: function() {
    clearTimeout(get_data_timer)
  },

  //功能0  ok
  B_bypass: function(){
    var that = this;
    Sendcmd("Bypass:1,", that)
  },
  B_ledon: function(){
    var that = this;
    Sendcmd("LED:0,", that)
  },
  B_ledoff: function(){
    var that = this;
    Sendcmd("LED:1,", that)
  },
  S_vol(e) {
    var that = this;
    this.setData({ Vol_value: e.detail.value })
    Sendcmd("VOL_num:"+that.data.Vol_value+",", that)
  },
  //功能1
  B_vsound: function(){
    var that = this;
    Sendcmd("V_sound:1,", that)
  },
  S_time(e) {
    var that = this;
    this.setData({ time_value: e.detail.value })
    Sendcmd("time:"+that.data.time_value+",", that)
  },
  S_range(e) {
    var that = this;
    this.setData({ range_value: e.detail.value })
    Sendcmd("range:"+that.data.range_value+",", that)
  },
  //功能2
  B_eq: function(){
    var that = this;
    Sendcmd("EQ:1,", that)
  },
  S_1fc(e) {
    var that = this;
    this.setData({ fc1_value: e.detail.value })
    Sendcmd("1fc:"+that.data.fc1_value+",", that)
  },
  S_1Q(e) {
    var that = this;
    this.setData({ Q1_value: e.detail.value })
    Sendcmd("1Q:"+that.data.Q1_value+",", that)
  },
  S_1G(e) {
    var that = this;
    this.setData({ G1_value: e.detail.value+50 })
    Sendcmd("1G:"+that.data.G1_value+",", that)
  },
  S_2fc(e) {
    var that = this;
    this.setData({ fc2_value: e.detail.value })
    Sendcmd("2fc:"+that.data.fc2_value+",", that)
  },
  S_2Q(e) {
    var that = this;
    this.setData({ Q2_value: e.detail.value })
    Sendcmd("2Q:"+that.data.Q2_value+",", that)
  },
  S_2G(e) {
    var that = this;
    this.setData({ G2_value: e.detail.value+50 })
    Sendcmd("2G:"+that.data.G2_value+",", that)
  },
//功能3
B_reverb: function(){
  var that = this;
  Sendcmd("Reverb:1,", that)
},
S_rt60(e) {
  var that = this;
  this.setData({ rt60_value: e.detail.value })
  Sendcmd("rt60:"+that.data.rt60_value+",", that)
},
S_alpha(e) {
  var that = this;
  this.setData({ alpha_value: e.detail.value })
  Sendcmd("alpha:"+that.data.alpha_value+",", that)
},

  Rslider(e) {
      this.setData({ Rslider_value: e.detail.value })
    // var that = this;
  },
  Gslider(e) {
      this.setData({ Gslider_value: e.detail.value })
    // var that = this;
  },
  Bslider(e) {
      this.setData({ Bslider_value: e.detail.value })
    // var that = this;
  },

  bindInput: function(e) {  //输入文本框
    this.setData({
      inputcmd: e.detail.value
    })
    //console.log("输入的数据：")
    //console.log(e.detail.value)
  },
  sendcmdkey(){
    var that = this;
    Sendcmd(that.data.inputcmd, that)
  },
  bindInput_id(e){
    this.setData({
      inputID: e.detail.value
    })
  }



})